export interface IPost {
  id?: number;
  text: string;
  user: number;
  inResponseTo?: number | { text: string };
  lang: string;
  createdAt?: string;
  updatedAt?: string;
}
