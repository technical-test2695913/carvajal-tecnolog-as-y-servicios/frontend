export interface IUser {
  name: string;
  lastName: string;
  email: string;
  password: string;
  birthDay: string;
  gender: number;
  country: string;
  city: string;
  address: string;
  lang?: string;
}