export interface IRequestChangePass {
  email: string;
  lang: string;
}

export interface IChangePass {
  changePassToken: string;
  password: string;
  confirmPassword: string;
}
