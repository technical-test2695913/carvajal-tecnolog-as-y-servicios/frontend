// types
import { IPost } from '../../../shared/interfaces/IPost'

export interface IPostCard {
  post: IPost;
  onEdit?: (data: IPost) => void;
  onDelete?: (id: number) => void;
  onReply?: (data: IPost) => void;
}