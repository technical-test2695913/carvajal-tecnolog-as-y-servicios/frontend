// Types
import { useTranslation } from "react-i18next";
import { postStore } from "../../../store";
import { IPostCard } from "./interface";

// Utils
import { sliceText } from "../../../utils/text";

import './styles.scss';

export const PostCard = ({
  post,
  onEdit,
  onDelete,
  onReply
}: IPostCard) => {
  const { t } = useTranslation();

  const handleDelete = () => {
    if (post.id) {
      postStore.deletePost(post.id);
      if (onDelete) onDelete(post.id);
    }
  }

  return (
    <article className="post-card shadow-sm">
      {post.inResponseTo && <p className="post-card__reply">
        <b><i>{t('POST.REPLY')}</i></b> <i>
          {sliceText((post.inResponseTo as any)?.text)}
        </i>
      </p>}
      <p className="mb-3">{post.text}</p>
      <section className="post-card__actions">
        <i
          className="bi bi-reply-fill"
          onClick={() => onReply && onReply(post)} />
        <i
          className="bi bi-pencil-square"
          onClick={() => onEdit && onEdit(post)} />
        <i
          className="bi bi-trash-fill"
          onClick={handleDelete} />
      </section>
      <p className="post-card__by">
        <b><i>{t('POST.BY')}</i></b> <i>
          {(post.user as any).name}
        </i>
      </p>
    </article>
  )
}