import { useState } from "react"
import { useTranslation } from "react-i18next"
import { Formik, FormikHelpers } from "formik"

// Types
import { IFormPost } from "./interface"

// Components
import { CardLink } from "../../card/CardLinks"

// Formik
import { initialValues, validationSchema } from "./form.formik"

// Components
import { Input } from "../../Input"

// Store
import { postStore } from "../../../store/postStore"

// Utils
import { sliceText } from "../../../utils/text"

export const FormPost = ({ onSave, onEdit, reply, post }: IFormPost) => {
  const { t, i18n: { language } } = useTranslation();
  const [error, setError] = useState<string>();

  const handleSubmit = async (values: any, helpers: FormikHelpers<any>) => {
    const data = {
      ...values,
      lang: language
    };
    if (reply) data.inResponseTo = reply.id;
    if (post) data.id = post.id;

    const res = post?.id ? await postStore.edit(data) : await postStore.create(data);

    if (res.message) return setError(res.message);
    else if (post?.id && onEdit) onEdit(reply ? ({ ...res, inResponseTo: reply }) : res)
    else if (onSave) onSave(reply ? ({ ...res, inResponseTo: reply }) : res);

    helpers.resetForm(initialValues as any);
  }

  return (
    <CardLink
      title={t('POST.TITLE')}>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}>
        {({ values, errors, handleSubmit, handleChange, touched }) => (
          <form
            onSubmit={handleSubmit}
            className={`p-3 row justify-content-center`}>
            {reply && <p className="post-card__reply">
              <b><i>{t('POST.REPLY')}</i></b> <i>
                {sliceText(reply.text)}
              </i>
            </p>}
            {post && <p className="post-card__reply">
              <b><i>{t('POST.EDIT')}</i></b> <i>
                {sliceText(post.text)}
              </i>
            </p>}
            {Object.keys(values).map((key: string, i) => (
              <Input
                key={key}
                name={key}
                className={`col-12`}
                label={t(`POST.${key.toUpperCase()}`)}
                onChange={handleChange}
                value={(values as any)[key]}
                error={(errors as any)[key] && t((errors as any)[key])}
                touched={(touched as any)[key]} />
            ))}

            <div className="col-12 text-center my-3">
              <button className="btn btn-primary" type="submit">Submit form</button>
            </div>
            {error && <div className="alert alert-danger text-center" role="alert">
              {t(`POST.${error}`)}
            </div>}
          </form>
        )}
      </Formik>
    </CardLink>
  )
}