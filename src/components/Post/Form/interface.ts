// Types
import { IPost } from "../../../shared/interfaces/IPost";

export interface IFormPost {
  post?: IPost;
  reply?: IPost;
  onSave?: (post: IPost) => void;
  onEdit?: (post: IPost) => void;
}