import * as Yup from "yup";

export const validationSchema = Yup.object({
  text: Yup.string()
    .required('POST.VALIDATIONS.TEXT.REQUIRED')
    .min(10, 'POST.VALIDATIONS.TEXT.MIN')
    .max(500, 'POST.VALIDATIONS.TEXT.MAX')
});

export const initialValues = {
  text: ''
};
