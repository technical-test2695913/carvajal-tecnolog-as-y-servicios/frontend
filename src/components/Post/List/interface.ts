// Types
import { IPost } from "../../../shared/interfaces/IPost";

export interface IListPost {
  posts?: IPost[];
  onEdit?: (data: IPost) => void;
  onDelete?: (id: number) => void;
  onReply?: (data: IPost) => void;
}