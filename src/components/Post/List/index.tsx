// Components
import { PostCard } from '../Card'

// Types
import { IListPost } from './interface';

export const ListPost = ({
  posts = [],
  onEdit,
  onDelete,
  onReply
}: IListPost) => {
  return (
    <>
      {posts.map(post => (
        <PostCard
          key={post.id}
          post={post}
          onEdit={onEdit}
          onDelete={onDelete}
          onReply={onReply} />
      ))}
    </>
  )
}
