import { ChangeEventHandler, HTMLInputTypeAttribute } from "react";

// Types
import { ISelector } from "../../shared/interfaces/ISelector";

export interface IInput {
  name?: string;
  id?: string;
  value?: any;
  type?: HTMLInputTypeAttribute | 'select' | 'autocomplete';
  label?: string | null;
  className?: string;
  onChange?: ChangeEventHandler<HTMLInputElement> | undefined
  error?: string | null;
  touched?: boolean;
  options?: ISelector[]
}