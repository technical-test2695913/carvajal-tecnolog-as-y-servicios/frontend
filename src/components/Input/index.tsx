import { useTranslation } from "react-i18next";
import { Field } from "formik";

// Types
import { IInput } from "./interface";

export const Input = ({
  type = 'text',
  value,
  name,
  id,
  label,
  className,
  onChange,
  error,
  touched,
  options = []
}: IInput) => {
  const { t, i18n: { language } } = useTranslation()

  const inputClassName = touched && error
    ? 'border-danger text-danger'
    : (touched ? 'border-success text-success' : '')
  const labelClassName = touched && error
    ? 'text-danger'
    : (touched ? 'text-success' : '')

  return (
    <div className={`form-floating mb-3 ${className}`}>
      {type === 'select'
        ? <Field
          as="select"
          value={value}
          className={`form-select ${inputClassName}`}
          name={name}
          id={id || name}
          placeholder={name}>
          <option value=''>{t('SELECT')}</option>
          {options.map(opt => (
            <option key={opt.id} value={opt.id}>
              {(opt as any)[language] || opt.code}
            </option>
          ))}
        </Field>
        : <input
          type={type === 'autocomplete' ? 'text' : type}
          value={value}
          className={`form-control ${inputClassName}`}
          name={name}
          id={id || name}
          placeholder={name}
          onChange={onChange}
          list={`list-${name}`} />}
      <datalist id={`list-${name}`}>
        {options.map(opt => (
          <option key={opt.id} value={opt.code} />
        ))}
      </datalist>
      <label htmlFor={id || name} className={`ps-3 ${labelClassName}`}>
        {label}
      </label>
      {error && touched && <div className="signup--error">
        {error}
      </div>}
    </div>
  )
}
