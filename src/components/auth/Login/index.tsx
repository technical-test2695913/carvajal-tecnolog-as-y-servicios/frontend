import { useNavigate } from "react-router-dom";
import { useState } from "react";
import { useTranslation } from "react-i18next";
import { Formik } from "formik";

// Types
import { ILogin } from "./interface";

// Components
import { CardLink } from "../../card/CardLinks";
import { Input } from "../../Input";

// Formik
import { initialValues, validationSchema } from "./form.formik";

// Store
import { store, userStore } from "../../../store";

import './styles.scss'

export const Login = ({ onChangePassword, onSignup }: ILogin) => {
  const { t } = useTranslation()
  const navigate = useNavigate()
  const [error, setError] = useState<string>()
  const types: any = {
    email: "email",
    password: "password"
  }

  const handleSubmit = async (values: any) => {
    const res = await userStore.login(values);
    if (res.message) setError(res.message);
    else {
      store.setToken(res.access_token);
      store.setUserId(res.id);
      store.setUserEmail(res.email);
      navigate("/post", { state: res });
    }
  }

  return (
    <CardLink
      title={t('LOGIN.TITLE')}
      actions={[
        { label: t('LOGIN.CHANGE'), action: onChangePassword },
        { label: t('LOGIN.SIGNUP'), action: onSignup }
      ]}>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}>
        {({ values, errors, handleSubmit, handleChange, touched }) => (
          <form
            onSubmit={handleSubmit}
            className={`p-3 row justify-content-center`}>
            {Object.keys(values).map((key: string, i) => (
              <Input
                key={key}
                name={key}
                type={types[key]}
                className={`col-12`}
                label={t(`SIGNUP.INPUTS.${key.toUpperCase()}`)}
                onChange={handleChange}
                value={(values as any)[key]}
                error={(errors as any)[key] && t((errors as any)[key])}
                touched={(touched as any)[key]} />
            ))}

            <div className="col-12 text-center my-3">
              <button className="btn btn-primary" type="submit">Submit form</button>
            </div>
            {error && <div className="alert alert-danger text-center" role="alert">
              {t(`LOGIN.${error}`)}
            </div>}
          </form>
        )}
      </Formik>
    </CardLink>
  )
}