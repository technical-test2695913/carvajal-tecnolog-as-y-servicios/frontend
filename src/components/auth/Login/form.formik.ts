import * as Yup from "yup";

export const validationSchema = Yup.object({
  email: Yup.string()
    .required('SIGNUP.VALIDATIONS.EMAIL.REQUIRED')
    .email('SIGNUP.VALIDATIONS.EMAIL.FORMAT')
    .min(8, 'SIGNUP.VALIDATIONS.EMAIL.MIN')
    .max(255, 'SIGNUP.VALIDATIONS.EMAIL.MAX'),
  password: Yup.string()
    .required('SIGNUP.VALIDATIONS.PASSWORD.REQUIRED')
    .matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
      'SIGNUP.VALIDATIONS.PASSWORD.FORMAT')
    .min(3, 'SIGNUP.VALIDATIONS.PASSWORD.MIN')
    .max(50, 'SIGNUP.VALIDATIONS.PASSWORD.MAX')
});

export const initialValues = {
  email: '',
  password: ''
};
