export interface ILogin {
  onChangePassword?: () => void;
  onSignup?: () => void;
}