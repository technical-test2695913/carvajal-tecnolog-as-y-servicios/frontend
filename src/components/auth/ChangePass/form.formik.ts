import * as Yup from "yup";

export const validationSchema = Yup.object({
  email: Yup.string()
    .required('SIGNUP.VALIDATIONS.EMAIL.REQUIRED')
    .email('SIGNUP.VALIDATIONS.EMAIL.FORMAT')
    .min(8, 'SIGNUP.VALIDATIONS.EMAIL.MIN')
    .max(255, 'SIGNUP.VALIDATIONS.EMAIL.MAX')
});

export const initialValues = {
  email: ''
};
