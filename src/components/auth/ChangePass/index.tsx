import { useTranslation } from "react-i18next"
import { Formik } from "formik"

// Types
import { IChangePass } from "./interface"

// Components
import { CardLink } from "../../card/CardLinks"

// Formik
import { initialValues, validationSchema } from "./form.formik"

// Components
import { Input } from "../../Input"

// Store
import { userStore } from "../../../store"

import './styles.scss'
import { useState } from "react"

export const ChangePass = ({ onSignup, onLogin }: IChangePass) => {
  const { t, i18n: { language } } = useTranslation();
  const [error, setError] = useState<string>();
  const [success, setSuccess] = useState(false);
  const types: any = {
    email: "email"
  }

  const handleSubmit = async (values: any) => {
    const res = await userStore.requestChangePass({
      ...values,
      lang: language
    })
    if (res.message) setError(res.message);
    else setSuccess(true);
  }

  return (
    <CardLink
      title={t('CHANGE_PASS.TITLE')}
      actions={[
        { label: t('CHANGE_PASS.SIGNUP'), action: onSignup },
        { label: t('CHANGE_PASS.LOGIN'), action: onLogin }
      ]}>
      {!success
        ? <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={handleSubmit}>
          {({ values, errors, handleSubmit, handleChange, touched }) => (
            <form
              onSubmit={handleSubmit}
              className={`p-3 row justify-content-center`}>
              {Object.keys(values).map((key: string, i) => (
                <Input
                  key={key}
                  name={key}
                  type={types[key]}
                  className={`col-12`}
                  label={t(`SIGNUP.INPUTS.${key.toUpperCase()}`)}
                  onChange={handleChange}
                  value={(values as any)[key]}
                  error={(errors as any)[key] && t((errors as any)[key])}
                  touched={(touched as any)[key]} />
              ))}

              <div className="col-12 text-center my-3">
                <button className="btn btn-primary" type="submit">Submit form</button>
              </div>
              {error && <div className="alert alert-danger text-center" role="alert">
                {t(`CHANGE_PASS.${error}`)}
              </div>}
            </form>
          )}
        </Formik>
        : <h6 className="m-3"><i>{t('CHANGE_PASS.MSG')}</i></h6>}
    </CardLink>
  )
}