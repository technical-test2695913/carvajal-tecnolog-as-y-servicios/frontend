import { useMemo, useState } from "react";
import { useTranslation } from "react-i18next";
import { Formik } from "formik";
import { observer } from "mobx-react";
import { City, State } from "country-state-city";
import { useNavigate } from "react-router-dom";

// Types
import { ISignup } from "./interface"
import { ISelector } from "../../../shared/interfaces/ISelector";

// Components
import { CardLink } from "../../card/CardLinks";
import { Input } from "../../Input";

// Formik
import { initialValues, validationSchema } from "./form.formik";

// Store
import { store, userStore } from "../../../store"

import './styles.scss';

export const Signup = observer(({ onChangePassword, onLogin }: ISignup) => {
  const { t, i18n: { language } } = useTranslation()
  const navigate = useNavigate();
  const [error, setError] = useState<string>()
  const types: any = {
    password: 'password',
    birthDay: 'date',
    gender: 'select',
    country: 'autocomplete',
    city: 'autocomplete'
  }

  const handleSubmit = async (values: any) => {
    const res = await userStore
      .saveUser({
        ...values,
        gender: parseInt(values.gender),
        lang: language
      });
    if (res.message) setError(res.message);
    else navigate("/user-created", { state: res });
  }

  const getCities = (country: string) => {
    country = country.split(',')[0];
    return City.getCitiesOfCountry(country)
      ?.map(({ name, stateCode }, i) => {
        const state = State.getStateByCodeAndCountry(stateCode, country)?.name;
        const city = `${name} (${state})`
        return ({ id: city, code: city });
      }) || []
  }

  const getInputClassName = (keys: string[], i: number): string => (
    `col-12 ${(i === keys.length - 1 && i % 2 === 0) ? '' : 'col-md-6'}`
  )

  return (
    <CardLink
      title={t('SIGNUP.TITLE')}
      actions={[
        { label: t('SIGNUP.CHANGE'), action: onChangePassword },
        { label: t('SIGNUP.LOGIN'), action: onLogin }
      ]}>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}>
        {({ values, errors, handleSubmit, handleChange, touched }) => {
          const [cities, setCities] = useState<ISelector[]>([])
          const selectors: any = {
            gender: store.genders,
            country: store.countries,
            city: cities
          }
          const keys = Object.keys(values)

          useMemo(() => setCities(getCities(values.country)), [values.country])

          return (
            <form
              onSubmit={handleSubmit}
              className={`p-3 row justify-content-center`}>
              {keys.map((key: string, i) => (
                <Input
                  key={key}
                  name={key}
                  type={types[key] || 'text'}
                  className={getInputClassName(keys, i)}
                  label={t(`SIGNUP.INPUTS.${key.toUpperCase()}`)}
                  onChange={handleChange}
                  value={(values as any)[key]}
                  error={(errors as any)[key] && t((errors as any)[key])}
                  touched={(touched as any)[key]}
                  options={selectors[key]} />
              ))}

              <div className="col-12 text-center my-3">
                <button className="btn btn-primary" type="submit">Submit form</button>
              </div>
              {error && <div className="alert alert-danger text-center" role="alert">
                {t(`SIGNUP.${error}`).replace('#EMAIL', values.email)}
              </div>}
            </form>
          )
        }}
      </Formik>
    </CardLink>
  )
})