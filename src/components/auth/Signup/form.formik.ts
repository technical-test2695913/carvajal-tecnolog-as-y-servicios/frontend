import * as Yup from "yup";

export const validationSchema = Yup.object({
  name: Yup.string()
    .required('SIGNUP.VALIDATIONS.NAME.REQUIRED')
    .min(3, 'SIGNUP.VALIDATIONS.NAME.MIN')
    .max(50, 'SIGNUP.VALIDATIONS.NAME.MAX'),
  lastName: Yup.string()
    .required('SIGNUP.VALIDATIONS.LASTNAME.REQUIRED')
    .min(3, 'SIGNUP.VALIDATIONS.LASTNAME.MIN')
    .max(50, 'SIGNUP.VALIDATIONS.LASTNAME.MAX'),
  email: Yup.string()
    .required('SIGNUP.VALIDATIONS.EMAIL.REQUIRED')
    .email('SIGNUP.VALIDATIONS.EMAIL.FORMAT')
    .min(8, 'SIGNUP.VALIDATIONS.EMAIL.MIN')
    .max(255, 'SIGNUP.VALIDATIONS.EMAIL.MAX'),
  password: Yup.string()
    .required('SIGNUP.VALIDATIONS.PASSWORD.REQUIRED')
    .matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
      'SIGNUP.VALIDATIONS.PASSWORD.FORMAT')
    .min(3, 'SIGNUP.VALIDATIONS.PASSWORD.MIN')
    .max(50, 'SIGNUP.VALIDATIONS.PASSWORD.MAX'),
  birthDay: Yup.string()
    .required('SIGNUP.VALIDATIONS.BIRTHDAY.REQUIRED'),
  gender: Yup.number()
    .required('SIGNUP.VALIDATIONS.GENDER.REQUIRED'),
  country: Yup.string()
    .required('SIGNUP.VALIDATIONS.COUNTRY.REQUIRED')
    .min(3, 'SIGNUP.VALIDATIONS.COUNTRY.MIN')
    .max(200, 'SIGNUP.VALIDATIONS.COUNTRY.MAX'),
  city: Yup.string()
    .required('SIGNUP.VALIDATIONS.CITY.REQUIRED')
    .min(3, 'SIGNUP.VALIDATIONS.CITY.MIN')
    .max(200, 'SIGNUP.VALIDATIONS.CITY.MAX'),
  address: Yup.string()
    .required('SIGNUP.VALIDATIONS.ADDRESS.REQUIRED')
    .min(3, 'SIGNUP.VALIDATIONS.ADDRESS.MIN')
    .max(500, 'SIGNUP.VALIDATIONS.ADDRESS.MAX'),
});

export const initialValues = {
  name: '',
  lastName: '',
  email: '',
  password: '',
  birthDay: '',
  gender: '',
  country: '',
  city: '',
  address: '',
};
