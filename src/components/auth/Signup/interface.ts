export interface ISignup {
  onChangePassword?: () => void;
  onLogin?: () => void;
}