import { useTranslation } from "react-i18next";

import "./styles.scss";

export const Langs = () => {
  const { i18n: { language, changeLanguage } } = useTranslation()
  return (
    <article className="langs">
      <section
        className={language === 'es' ? 'active' : ''}
        onClick={() => changeLanguage('es')}>ES</section>
      <section
        className={language === 'en' ? 'active' : ''}
        onClick={() => changeLanguage('en')}>EN</section>
    </article>
  )
}