import { createBrowserRouter } from "react-router-dom";

// Pages
import { Home } from "./pages/Home";
import { Post } from "./pages/Post";
import { UserCreated } from "./pages/UserCreated";
import { VerifyUser } from "./pages/VerifyUser";
import { ChangePass } from "./pages/ChangePass";

export const router = createBrowserRouter([
  {
    path: "/",
    Component: Home
  },
  {
    path: "/post",
    Component: Post,
  },
  {
    path: "/user-created",
    Component: UserCreated,
  },
  {
    path: "/verify-user",
    Component: VerifyUser,
  },
  {
    path: "/change-pass",
    Component: ChangePass,
  },
]);