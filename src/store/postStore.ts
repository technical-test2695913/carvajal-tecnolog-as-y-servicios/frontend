// Store
import { Store, store } from "./store";

// Types
import { IPost } from "../shared/interfaces/IPost";

class PostStore {
  store?: Store = undefined;

  constructor(store: Store) {
    this.store = store;
  }

  create(post: IPost) {
    return this.store?.post('/post', {
      ...post,
      user: this.store.userId
    });
  }

  async getAll() {
    return await this.store?.get('/post');
  }

  edit(post: IPost) {
    console.log(post)
    return this.store?.put(`/post/${post.id}`, post);
  }

  deletePost(id: number) {
    return this.store?.delete(`/post/${id}`);
  }
}

export const postStore = new PostStore(store);
