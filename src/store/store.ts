import { makeAutoObservable } from "mobx";
import env from "react-dotenv";

// Types
import { ISelector } from "../shared/interfaces/ISelector";
import { Country } from "country-state-city";

export class Store {
  private token: string | null = null;
  userId: number | null = null;
  uesrEmail: string | null = null;
  error: string | null = null;
  genders: ISelector[] = [];
  identificationTypes: ISelector[] = [];
  countries: ISelector[] = [];
  loading = false;

  constructor() {
    makeAutoObservable(this);

    this.get('/gender').then(res => this.genders = res)
    this.get('/identification-type').then(res => this.identificationTypes = res)
    this.countries = Country.getAllCountries().map(({ name, isoCode }) =>
      ({ id: isoCode, code: `${isoCode}, ${name}` }))
  }

  setToken(token: string | null) {
    this.token = token;
  }

  setUserId(id: number | null) {
    this.userId = id;
  }

  setUserEmail(email: string | null) {
    this.uesrEmail = email;
  }

  setLoading(loading: boolean) {
    this.loading = loading;
  }

  get isAuthenticated() {
    return Boolean(this.token);
  }

  private get headers() {
    var hds = new Headers();
    hds.append("accept", "*/*");
    hds.append("Cookie", "cf_ob_info=504:7b12b307ff65f78a:BOG; cf_use_ob=443")
    hds.append("Content-Type", "application/json")
    if (this.token) hds.append("Authorization", `Bearer ${this.token}`);

    return hds;
  }

  private async apiMiddleware(
    url: string,
    init?: RequestInit | undefined
  ): Promise<any> {
    try {
      this.setLoading(true);
      const response = await fetch(`${env.API_URL}${url}`, init);
      const result = await response.json();
      this.setLoading(false);
      return result?.data || result;
    } catch (error) {
      this.error = (error as any)?.message;
      return null;
    }
  }

  get(url: string) {
    return this.apiMiddleware(url, {
      method: 'GET',
      headers: this.headers
    })
  }

  post(url: string, body: any = {}) {
    return this.apiMiddleware(url, {
      method: 'POST',
      headers: this.headers,
      body: JSON.stringify(body)
    })
  }

  put(url: string, body: any = {}) {
    return this.apiMiddleware(url, {
      method: 'PUT',
      headers: this.headers,
      body: JSON.stringify(body)
    })
  }

  delete(url: string) {
    return this.apiMiddleware(url, {
      method: 'DELETE',
      headers: this.headers
    })
  }
}

export const store = new Store();
