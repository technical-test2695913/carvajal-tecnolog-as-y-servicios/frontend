import { useEffect } from "react"
import { useTranslation } from "react-i18next"
import { useNavigate, useSearchParams } from "react-router-dom"

// Loyout
import { MainLayout } from "../../layouts/Main"
// Components
import { CardLink } from "../../components/card/CardLinks"

// Store
import { store, userStore } from "../../store"

export const VerifyUser = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();

  const handleVerify = async () => {
    const token = searchParams.get('userVerify');
    if (token) await userStore.verify(token);
    navigate('/')
  }

  useEffect(() => {
    if (store.isAuthenticated) navigate('/post');
  }, [navigate])

  return (
    <MainLayout>
      <CardLink
        title={t('VERIFY_USER.TITLE')}
        className="col-12 col-sm-10 col-md-6 col-lg-4"
        actions={[
          { label: t('VERIFY_USER.ACTION'), action: handleVerify }
        ]}>
        <div className="m-3">
          <h6><i>{t('VERIFY_USER.MSG')}</i></h6>
        </div>
      </CardLink>
    </MainLayout>
  )
}
