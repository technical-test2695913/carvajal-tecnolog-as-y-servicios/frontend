import { useEffect, useState } from "react";
import { observer } from "mobx-react";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";

// Layout
import { MainLayout } from "../../layouts/Main";

// Store
import { postStore, store } from "../../store";

// Components
import { FormPost } from "../../components/Post/Form";
import { ListPost } from "../../components/Post/List";

// Types
import { IPost } from "../../shared/interfaces/IPost";

// Assets
import gif from '../../assets/premade.gif';

export const Post = observer(() => {
  const navigate = useNavigate();
  const { t } = useTranslation();
  const [posts, setPosts] = useState<IPost[]>([]);
  const [reply, setReply] = useState<IPost>();
  const [post, setPost] = useState<IPost>();

  const handleSave = (post: IPost) => {
    setPosts([post, ...posts]);
    setReply(undefined);
  }

  const handleEdit = (newPost: IPost) => {
    getData();
    setReply(undefined);
    setPost(undefined);
  }

  const getData = async () => {
    const res = await postStore.getAll();
    setPosts(res || []);
  }

  useEffect(() => {
    if (!store.isAuthenticated) navigate('/');
  }, [navigate])

  useEffect(() => {
    getData();
  }, [])

  return (
    <MainLayout>
      <section className="row col-12 col-md-10">
        <section className="col-12 col-lg-4">
          <FormPost onSave={handleSave} onEdit={handleEdit} reply={reply} post={post} />
        </section>
        <section className="col-12 col-lg-8 d-flex flex-column justify-content-center align-items-center">
          {posts.length > 0
            ? <ListPost
              posts={posts}
              onEdit={post => setPost(post)}
              onDelete={id => setPosts(posts.filter(p => p.id !== id))}
              onReply={post => setReply(post)} />
            : <>
              <img className="w-50 mt-5 mt-lg-0" src={gif} alt="Mi gif" />
              <h6><i>{t('POST.NOT_DATA')}</i></h6>
            </>}
        </section>
      </section>
    </MainLayout>
  )
})
