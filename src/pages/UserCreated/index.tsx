import { useEffect } from "react"
import { useTranslation } from "react-i18next"
import { useLocation, useNavigate } from "react-router-dom"

// Loyout
import { MainLayout } from "../../layouts/Main"

// Components
import { CardLink } from "../../components/card/CardLinks"

// Store
import { store } from "../../store"

export const UserCreated = () => {
  const { t } = useTranslation();
  const { state: { id, name, lastname } } = useLocation();
  const navigate = useNavigate();

  useEffect(() => {
    if (store.isAuthenticated) navigate('/post');
  }, [navigate])

  useEffect(() => {
    if (!id) navigate('/');
  }, [id, navigate])

  return (
    <MainLayout>
      <CardLink
        title={t('USER_CREATED.TITLE')}
        className="col-12 col-sm-10 col-md-6 col-lg-4"
        actions={[
          { label: t('SIGNUP.LOGIN'), action: () => navigate('/') }
        ]}>
        <div className="m-3">
          <h4>{[name, lastname].join(' ')}</h4>
          <h6><i>{t('USER_CREATED.MSG')}</i></h6>
        </div>
      </CardLink>
    </MainLayout>
  )
}
