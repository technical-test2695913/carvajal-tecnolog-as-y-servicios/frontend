import { useNavigate } from "react-router-dom";

// Components
import { useEffect, useState } from "react";

// Components
import { Login } from "../../components/auth/Login";
import { Signup } from "../../components/auth/Signup";
import { ChangePass } from "../../components/auth/ChangePass";

// Layout
import { MainLayout } from "../../layouts/Main";

// Store
import { store } from "../../store";

import './styles.scss';

export const Home = () => {
  const [current, setCurrent] = useState<'login' | 'signup' | 'change'>('login');
  const navigate = useNavigate();

  useEffect(() => {
    if (store.isAuthenticated) navigate('/post');
  }, [navigate])

  return (
    <MainLayout>
      <div className="home col-12 col-sm-10 col-md-6 col-lg-4">
        {current === 'login' && <Login
          onChangePassword={() => setCurrent('change')}
          onSignup={() => setCurrent('signup')} />}
        {current === 'signup' && <Signup
          onChangePassword={() => setCurrent('change')}
          onLogin={() => setCurrent('login')} />}
        {current === 'change' && <ChangePass
          onSignup={() => setCurrent('signup')}
          onLogin={() => setCurrent('login')} />}
      </div>
    </MainLayout>
  )
}
