import * as Yup from "yup";

export const validationSchema = Yup.object({
  password: Yup.string()
    .required('SIGNUP.VALIDATIONS.PASSWORD.REQUIRED')
    .matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
      'SIGNUP.VALIDATIONS.PASSWORD.FORMAT')
    .min(3, 'SIGNUP.VALIDATIONS.PASSWORD.MIN')
    .max(50, 'SIGNUP.VALIDATIONS.PASSWORD.MAX'),
  confirmPassword: Yup.string()
    .required('CHANGE_PASS.VALIDATIONS.CONFIRMPASSWORD.REQUIRED')
    .oneOf([Yup.ref('password')], 'CHANGE_PASS.VALIDATIONS.CONFIRMPASSWORD.SAME')
});

export const initialValues = {
  password: '',
  confirmPassword: ''
};
