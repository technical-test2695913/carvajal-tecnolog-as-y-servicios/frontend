export interface IChangePass {
  onSignup?: () => void;
  onLogin?: () => void;
}