export const LOGIN = {
  TITLE: 'Iniciar sesión',
  CHANGE: 'Cambiar contraseña',
  SIGNUP: 'Crear usuario',
  UNATHORIZED: 'Usuario no autorizado, correo o contraseña incorrecta',
  VAL_USER_NOT_VERIFIED: 'Por favor verifica tu cuenta, revisa tu correo.'
}