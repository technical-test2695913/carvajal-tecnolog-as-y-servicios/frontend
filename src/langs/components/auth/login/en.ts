export const LOGIN = {
  TITLE: 'Log in',
  CHANGE: 'Change password',
  SIGNUP: 'Sign up',
  UNATHORIZED: 'Unauthorized user, wrong email or password',
  VAL_USER_NOT_VERIFIED: 'Please verify your account, check your email.'
}