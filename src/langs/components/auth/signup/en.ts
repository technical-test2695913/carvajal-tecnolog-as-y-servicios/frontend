export const SIGNUP = {
  TITLE: 'Sign up',
  CHANGE: 'Change password',
  LOGIN: 'Log in',
  VAL_USER_ALREADY_REGISTERED_WITH_EMAIL: 'The user with the email #EMAIL already exists',
  INPUTS: {
    NAME: 'Name',
    LASTNAME: 'Last name',
    EMAIL: 'Email',
    PASSWORD: 'Password',
    CONFIRMPASSWORD: 'Confirm the password',
    BIRTHDAY: 'Birth day',
    GENDER: 'Gender',
    COUNTRY: 'Country',
    CITY: 'City',
    ADDRESS: 'Address'
  },
  VALIDATIONS: {
    NAME: {
      REQUIRED: 'The name is required',
      MIN: 'The name must have at least 3 characters',
      MAX: 'The name must have a maximum of 50 characters'
    },
    LASTNAME: {
      REQUIRED: 'The last name is required',
      MIN: 'The last name must have at least 3 characters',
      MAX: 'The last name must have a maximum of 50 characters'
    },
    EMAIL: {
      REQUIRED: 'The email is required',
      FORMAT: 'The email format is not correct',
      MIN: 'The email must have at least 3 characters',
      MAX: 'The email must have a maximum of 255 characters'
    },
    PASSWORD: {
      REQUIRED: 'The password is required',
      FORMAT: 'Password must be at least 8 characters long and contain at least one uppercase letter, one lowercase letter, one number, and one special character',
      MIN: 'The email must have at least 8 characters',
      MAX: 'The email must have a maximum of 128 characters'
    },
    BIRTHDAY: {
      REQUIRED: 'The birth day is required'
    },
    GENDER: {
      REQUIRED: 'The gender is required'
    },
    COUNTRY: {
      REQUIRED: 'The country is required',
      MIN: 'The country must have at least 3 characters',
      MAX: 'The country must have a maximum of 200 characters'
    },
    CITY: {
      REQUIRED: 'The city is required',
      MIN: 'The city must have at least 3 characters',
      MAX: 'The city must have a maximum of 200 characters'
    },
    ADDRESS: {
      REQUIRED: 'The address is required',
      MIN: 'The address must have at least 3 characters',
      MAX: 'The address must have a maximum of 500 characters'
    }
  }
}