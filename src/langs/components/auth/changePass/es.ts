export const CHANGE_PASS = {
  TITLE: 'Cambiar contraseña',
  SIGNUP: 'Crear usuario',
  LOGIN: 'Iniciar sesión',
  MSG: '¡Correcto!, a tu correo te enviamos un link de acceso para que puedas realizar el cambio de contraseña.',
  MSG_END: '¡Contraseña cambiada correctamente!',
  VAL_TOKEN_NOT_FOUND: 'Token invalido',
  UNEXPECTED: 'Error inesperado, por favor intentalo mas tarde de nuevo',
  VAL_PASS_NOT_MATCH: 'Las contraseñas no coinciden',
  VAL_USER_NOT_EXIST_WITH_THIS_EMAIL: 'No existe un usuario registrado con este correo',
  INPUTS: {
    CONFIRMPASSWORD: 'Confirma la contraseña'
  },
  VALIDATIONS: {
    CONFIRMPASSWORD: {
      REQUIRED: 'La confirmación de la contraseña es obligatoria',
      SAME: 'Las contraseñas no coinciden',
    }
  }
}