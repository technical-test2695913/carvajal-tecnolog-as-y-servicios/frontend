// Components
import { LOGIN } from './components/auth/login/en'
import { SIGNUP } from './components/auth/signup/en'
import { CHANGE_PASS } from './components/auth/changePass/en'

export const en = {
  translation: {
    APP: 'Social Carvajal',
    SELECT: 'Choose ...',
    LOGIN,
    SIGNUP,
    CHANGE_PASS,
    USER_CREATED: {
      TITLE: 'User created successfully',
      MSG: 'Welcome, the user was created successfully, please go to the email that I provided so that you can activate your account.'
    },
    VERIFY_USER: {
      TITLE: 'Verify user',
      MSG: 'Welcome, please click the button below to verify your account.',
      ACTION: 'Verify'
    },
    POST: {
      TITLE: 'Add new post',
      NOT_DATA: 'No post yet',
      TEXT: 'Write your post here',
      REPLY: 'In response to',
      EDIT: 'Editing',
      BY: 'By',
      VALIDATIONS: {
        TEXT: {
          REQUIRED: 'The text is required',
          MIN: 'The text must have at least 10 characters',
          MAX: 'The text must have a maximum of 500 characters'
        },
      }
    }
  }
}