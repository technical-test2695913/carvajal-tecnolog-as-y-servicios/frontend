// Components
import { LOGIN } from './components/auth/login/es'
import { SIGNUP } from './components/auth/signup/es'
import { CHANGE_PASS } from './components/auth/changePass/es'

export const es = {
  translation: {
    APP: 'Social Carvajal',
    SELECT: 'Selecciona ...',
    LOGIN,
    SIGNUP,
    CHANGE_PASS,
    USER_CREATED: {
      TITLE: 'Usuario creado con éxito',
      MSG: '¡Bienvenido!, el usuario fue creado con éxito, por favor diríjase al correo electrónico que facilito para que pueda activar su cuenta.'
    },
    VERIFY_USER: {
      TITLE: 'Verificar usuario',
      MSG: 'Bienvenido, por favor dale click al siguiente botón para verificar su cuenta.',
      ACTION: 'Verificar'
    },
    POST: {
      TITLE: 'Agrega un nuevo post',
      NOT_DATA: 'Aún no hay post',
      TEXT: 'Escribe tu post aquí',
      REPLY: 'En respuesta a',
      EDIT: 'Editando',
      BY: 'Por',
      VALIDATIONS: {
        NAME: {
          REQUIRED: 'El texto es requerido',
          MIN: 'El texto debe tener al menos 10 caracteres',
          MAX: 'El texto debe tener maximo 500 caracteres'
        }
      }
    }
  }
}