export const sliceText = (text: string) => text.slice(0, 50) + (text.length > 20 ? '...' : '');
