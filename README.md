## Descripción

Este es el frontend realizado para la interacción con la autenticación y el sistema de posteo.

## Variables de entorno

Es muy importante que en el archivo `.env` que lo encuentra en la raiz de este proyecto ponga la url del servicio de backend desplegado segun sea el caso

## Installation

```bash
$ npm install
```

## Running the app

```bash
$ npm run start
```

## Build the app

```bash
$ npm run build
```

## Test

```bash
# unit tests
$ npm run test
```

Con esto debe ser capaz de poder ejecutar la aplicación y empezarla a usar.